<?php

namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class AuthCheck implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {

        $session = \Config\Services::session();
        $usersModel = new \App\Models\UsersModel();

        if (!$session->has('token')) {

            $session->destroy();
            return redirect()->to('/login?msg=Unauthorized Access');

        } else {

            $tokenCheck = $usersModel->tokenCheck($session->token, $session->user_id);

            if($tokenCheck){

                $currentTime = time();

                if($tokenCheck->expired < $currentTime){

                    $usersModel->deactivateToken($session->token, $session->user_id);
                    $session->destroy();
                    return redirect()->to('/login?msg=Your Session is Expired');

                }

            } else {

                return redirect()->to('/login?msg=Invalid Token');

            }

        }
        
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }
}
