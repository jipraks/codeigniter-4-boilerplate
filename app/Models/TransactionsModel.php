<?php

namespace App\Models;
use CodeIgniter\Model;

class TransactionsModel extends Model {

    public function getTransactions($config) {

        $db = \Config\Database::connect();
        $query = $db->query("SELECT * FROM view_transactions_header WHERE status NOT IN ('EXPIRED', 'NEW') AND createtime BETWEEN '".$config['startDate']."' AND '".$config['endDate']."' ORDER BY createtime DESC");
        
        $result = $query->getResult();

        if(count($result) > 0){

            return $result;

        } else {

            return [];

        }

    }

    
}