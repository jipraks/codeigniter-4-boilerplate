<?php

namespace App\Models;
use CodeIgniter\Model;

class ProductsModel extends Model {

    public function getProducts() {

        $db = \Config\Database::connect();
        $getProductQuery = "SELECT * FROM view_products";
        $getProductExec = $db->query($getProductQuery);

        $getProductResults = $getProductExec->getResult();

        if(count($getProductResults) > 0){

            return $getProductResults;

        } else {

            return [];

        }
        
        return true;
    
    }

    
}