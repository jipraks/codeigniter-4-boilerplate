<?php

namespace App\Models;

use CodeIgniter\Model;

class UsersModel extends Model
{

    public function userLogin($username, $password)
    {

        $db = \Config\Database::connect();

        $sql = "SELECT * FROM webmin_users WHERE username = :username: AND password = ENCODE(HMAC(:password:, salt, 'sha512'),'hex') AND is_active = :is_active:;";
        $query = $db->query($sql, [
            "username" => $username,
            "password" => $password,
            "is_active" => true
        ]);

        $result = $query->getResult();

        if (count($result) > 0) {

            $currentTime = time();
            $tokenExpiredTime = $currentTime + (60 * 7200);
            $userToken = md5(uniqid($result[0]->salt, true));
            $sessionId = bin2hex(openssl_random_pseudo_bytes(10));

            $insertTokenQuery   = "INSERT INTO webmin_sessions(id, user_id, token, expired, is_active) VALUES(:id:, :user_id:, :token:, :expired:, :is_active:)";
            $insertTokenExec    = $db->query($insertTokenQuery, [
                "id" => $sessionId,
                "user_id" => $result[0]->id,
                "token" => $userToken,
                "expired" => $tokenExpiredTime,
                "is_active" => true
            ]);

            if ($db->affectedRows() > 0) {

                $updateLastLoginQuery   = "UPDATE webmin_users SET last_login = NOW() WHERE id = :user_id:";
                $updateLastLoginExec    = $db->query($updateLastLoginQuery, ["user_id" => $result[0]->id]);

                $userData = [
                    "user_id" => $result[0]->id,
                    "token" => $userToken,
                    "expired" => $tokenExpiredTime,
                    "fullname" => $result[0]->fullname,
                    "username" => $result[0]->username,
                    "role" => $result[0]->role
                ];

                return $userData;
            } else {

                return false;
            }
        } else {

            return false;
        }
    }

    public function tokenCheck($token, $user_id)
    {

        $db = \Config\Database::connect();

        $getSessionQuery    = "SELECT * FROM webmin_sessions WHERE user_id = :user_id: AND token = :token: AND is_active = :is_active:";
        $getSessionExec     = $db->query($getSessionQuery, [
            "user_id" => $user_id,
            "token" => $token,
            "is_active" => true
        ]);

        $getSessionResult = $getSessionExec->getResult();

        if (count($getSessionResult) > 0) {

            return $getSessionResult[0];
        } else {

            return false;
        }
    }

    public function deactivateToken($token, $user_id)
    {

        $db = \Config\Database::connect();

        $updateSessionQuery    = "UPDATE webmin_sessions SET is_active = false WHERE user_id = :user_id: AND token = :token: AND is_active = :is_active:";
        $updateSessionExec     = $db->query($updateSessionQuery, [
            "user_id" => $user_id,
            "token" => $token,
            "is_active" => true
        ]);

        return true;
    }

    public function getUsers()
    {

        $db = \Config\Database::connect();

        $getUsersQuery    = "SELECT * FROM webmin_users";
        $getUsersExec     = $db->query($getUsersQuery);

        $getUsersResult   = $getUsersExec->getResult();

        if (count($getUsersResult) > 0) {
            return $getUsersResult;
        } else {
            return [];
        }
    }

    public function createUser($d)
    {

        $db = \Config\Database::connect();

        $userId = bin2hex(openssl_random_pseudo_bytes(10));
        $userSalt = bin2hex(openssl_random_pseudo_bytes(10));

        $createUserQuery = "INSERT INTO webmin_users(id, fullname, username, salt, password, role, last_login, createtime, updatetime, is_active) VALUES(:user_id:, :fullname:, :username:, :salt:, ENCODE(HMAC(:password:, :salt:, 'sha512'),'hex'), :role:, null, NOW(), NOW(), true)";

        try {

            $createUserExec = $db->query($createUserQuery, [
                "user_id" => $userId,
                "fullname" => $d['fullname'],
                "username" => $d['username'],
                "salt" => $userSalt,
                "password" => $d['password_1'],
                "role" => $d['role']
            ]);

            if ($createUserExec) {

                if ($db->affectedRows() > 0) {

                    return true;
                } else {

                    return false;
                }
            } else {

                return false;
            }
        } catch (\Exception $e) {

            return false;
        }
    }

    public function activateUser($user_id){

        $db = \Config\Database::connect();
        $activateUserQuery = "UPDATE webmin_users SET is_active = :is_active:, updatetime = NOW() WHERE id = :user_id:";
        $activateUserExec = $db->query($activateUserQuery, [
            "is_active" => true,
            "user_id" => $user_id
        ]);

        return true;

    }

    public function deactivateUser($user_id){

        $db = \Config\Database::connect();
        $deactivateUserQuery = "UPDATE webmin_users SET is_active = :is_active:, updatetime = NOW() WHERE id = :user_id:";
        $deactivateUserExec = $db->query($deactivateUserQuery, [
            "is_active" => false,
            "user_id" => $user_id
        ]);

        return true;

    }

}
