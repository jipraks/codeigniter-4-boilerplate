<?php

namespace App\Controllers;

class Home extends BaseController
{

	public function index()
	{

		$transactionsModel = new \App\Models\TransactionsModel();
		$transactionsData = $transactionsModel->getTransactions($config);

		$page = 'Dashboard';
		$pageData = "";
		$data['pageTitle'] = $page . ' | CodeIgniter 4 Boilerplate';
		$data['activeNav'] = $page;
		$data['content'] = view('home', $pageData);

		return view('base_view', $data);
	}
}
