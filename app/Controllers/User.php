<?php

namespace App\Controllers;

class User extends BaseController
{

    public function index()
    {

        $usersModel = new \App\Models\UsersModel();
        $usersData = $usersModel->getUsers();

        $pageData['usersData'] = $usersData;

        $page = 'Users';

        $data['pageTitle'] = $page . ' | IKI Mitra Webmin';
        $data['activeNav'] = $page;
        $data['content'] = view('user', $pageData);

        return view('base_view', $data);
        
    }

    public function createUser(){

        if( isset($_POST['username']) && isset($_POST['fullname']) && isset($_POST['password_1']) && isset($_POST['password_2']) && isset($_POST['role']) ){

            if($_POST['password_1'] != $_POST['password_2']){

                return redirect()->to('/user?msg=Password Does not Match&type=error');

            }

            $usersModel = new \App\Models\UsersModel();
            $userCreate = $usersModel->createUser($_POST);

            if($userCreate){

                return redirect()->to('/user?msg=User Created Successfully&type=success');

            } else {

                return redirect()->to('/user?msg=System Error&type=error');

            }

        } else {

            redirect()->to('/user?msg=Invalid Action&type=error');

        }

    }

    public function activateUser($user_id){

        $usersModel = new \App\Models\UsersModel();
        $usersModel->activateUser($user_id);

        return redirect()->to('/user?msg=User Activated Successfully&type=success');

    }

    public function deactivateUser($user_id){

        $usersModel = new \App\Models\UsersModel();
        $usersModel->deactivateUser($user_id);

        return redirect()->to('/user?msg=User Deactivated Successfully&type=success');

    }

}
