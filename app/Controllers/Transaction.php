<?php

namespace App\Controllers;

class Transaction extends BaseController
{

    public function index()
    {

        $config = [];
        $config['startDate'] = date('Y-m-d') . ' 00:00:00';
        $config['endDate'] = date('Y-m-d') . ' 23:59:59';

        if (isset($_GET['status'])) {

            if ($_GET['status'] != 'ALL') {

                $config['status'] = $_GET['status'];
            }
        }

        if (isset($_GET['startDate'])) {

            if ($_GET['startDate'] != '') {

                $config['startDate'] = $_GET['startDate'] . ' 00:00:00';
            }
        }

        if (isset($_GET['endDate'])) {

            if ($_GET['endDate'] != '') {

                $config['endDate'] = $_GET['endDate'] . ' 23:59:59';
            }
        }

        $transactionsModel = new \App\Models\TransactionsModel();
        $transactionsData = $transactionsModel->getTransactions($config);

        $pageData['transactionsData'] = $transactionsData;
        $pageDate['startDate'] = $config['startDate'];
        $pageDate['endDate'] = $config['endDate'];

        $page = 'Transactions';

        $data['pageTitle'] = $page . ' | IKI Mitra Webmin';
        $data['activeNav'] = $page;
        $data['content'] = view('transaction', $pageData);

        return view('base_view', $data);
        
    }
}
