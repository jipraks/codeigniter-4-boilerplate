<?php

namespace App\Controllers;

class Product extends BaseController
{

	public function index()
	{

        $productsModel = new \App\Models\ProductsModel();
        $productsData = $productsModel->getProducts();

        $pageData['productsData'] = $productsData;

		$page = 'Products';

		$data['pageTitle'] = $page . ' | IKI Mitra Webmin';
		$data['activeNav'] = $page;
		$data['content'] = view('product', $pageData);

		return view('base_view', $data);
	}
}
