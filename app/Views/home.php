<div class="card">

    <div class="card-header card-header-primary">
        <h4 class="card-title">Today Transaction Summary</h4>
    </div>

    <div class="container-fluid mt-4 mb-2">

        <div class="row">

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-primary card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">content_copy</i>
                        </div>
                        <p class="card-category">Total Transaction</p>
                        <h3 class="card-title"><?= $transactionSummary['today']; ?>
                            <small>Trxs</small>
                        </h3>
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
            </div>


            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-success card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">done</i>
                        </div>
                        <p class="card-category">Success Transaction</p>
                        <h3 class="card-title"><?= $transactionSummary['success']; ?>
                            <small>Trxs</small>
                        </h3>
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-warning card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">autorenew</i>
                        </div>
                        <p class="card-category">Pending Transaction</p>
                        <h3 class="card-title"><?= $transactionSummary['pending']; ?>
                            <small>Trxs</small>
                        </h3>
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-danger card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">clear</i>
                        </div>
                        <p class="card-category">Failed Transaction</p>
                        <h3 class="card-title"><?= $transactionSummary['failed']; ?>
                            <small>Trxs</small>
                        </h3>
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>