<div class="row">
   <div class="col-md-12">
      <div class="card">
         <div class="card-header card-header-primary">
            <h4 class="card-title">Transaction</h4>
            <p class="card-category">Transaction Monitoring</p>
         </div>
         <div class="card-body">
            <form action="" method="GET">
               <div class="row">
                  <div class="col-md-3 mt-1">
                     <label>Status Filter</label>
                     <select class="form-control" name="status" id="transactionStatusSelector">
                        <option value="ALL">== Filter Status ==</option>
                        <option value="SUCCESS">Success</option>
                        <option value="PENDING">Pending</option>
                        <option value="FAILED">Failed</option>
                     </select>
                  </div>
                  <div class="col-md-3">
                     <label>Start Date</label>
                     <input type="date" class="form-control" id="startDatePicker" value="<?=$startDate;?>" name="startDate" placeholder="Start Date" />
                  </div>
                  <div class="col-md-3">
                     <label>End Date</label>
                     <input type="date" class="form-control" id="startDatePicker" value="<?=$endDate;?>" name="endDate" placeholder="Start Date" />
                  </div>
                  <div class="col-md-3">
                     <button type="submit" class="btn btn-primary mt-3">Filter</button>
                  </div>
               </div>
            </form>
            <div class="row">
               <div class="col-md-12">
                  <table id="transactionsTable" width="100%" class="display">
                     <thead>
                        <tr>
                           <th>Status</th>
                           <th>#ID</th>
                           <th>Zonid</th>
                           <th>Product ID</th>
                           <th>Product</th>
                           <th>Category ID</th>
                           <th>Customer ID</th>
                           <th>Base Price</th>
                           <th>Selling Price</th>
                           <th>Upline Markup</th>
                           <th>Create Time</th>
                           <th>Update Time</th>
                        </tr>
                     </thead>
                     <tbody>

                        <?php if(count($transactionsData) > 0) { ?>

                        <?php foreach($transactionsData AS $t) { ?>
                        <?php 
                           if($t->status == 'SUCCESS'){
                               $label = "<span class='badge badge-success'>".$t->status."</span>";
                           } else if($t->status == 'PENDING'){
                               $label = "<span class='badge badge-info'>".$t->status."</span>";
                           }  else if($t->status == 'FAILED'){
                               $label = "<span class='badge badge-danger'>".$t->status."</span>";
                           } else {
                               $label = "<span class='badge badge-warning'>".$t->status."</span>";
                           }
                           
                           ?>  
                        <tr>
                           <td><?=$label;?></td>
                           <td>
                              <a href="#">
                              <?=$t->transaction_id;?>
                              </a>
                           </td>
                           <td><?=$t->zonid;?></td>
                           <td><?=$t->product_id;?></td>
                           <td><?=$t->product_name;?></td>
                           <td><?=$t->category_id;?></td>
                           <td><?=$t->customer_id;?></td>
                           <td><?=number_format((int)$t->base_price,0);?></td>
                           <td><?=number_format((int)$t->selling_price,0);?></td>
                           <td><?=number_format((int)$t->upline_markup,0);?></td>
                           <td><?=$t->createtime;?></td>
                           <td><?=$t->updatetime;?></td>
                        </tr>
                        <?php } ?>


                        <?php } else { ?>
                        <tr>
                            <td colspan="12">No Data Available</td>
                        </tr>
                        <?php } ?>

                     </tbody>
                  </table>
               </div>
            </div>
            <div class="clearfix"></div>
            </form>
         </div>
      </div>
   </div>
</div>