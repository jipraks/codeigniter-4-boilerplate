<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Products</h4>
                <p class="card-category">Products Management</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="productsTable" width="100%" class="display">
                            <thead>
                                <tr>
                                    <th>Product ID</th>
                                    <th>Product Name</th>
                                    <th>Product Active</th>
                                    <th>Category ID</th>
                                    <th>Category Name</th>
                                    <th>Category Active</th>
                                    <th>Product Data</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php if (count($productsData) > 0) { ?>

                                    <?php foreach ($productsData as $t) { ?>

                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <?= $t->product_id; ?>
                                                </a>
                                            </td>
                                            <td><?= $t->product_name; ?></td>
                                            <td>
                                                <?php if ($t->is_product_active == 't') { ?>
                                                    <i class="material-icons">done</i>
                                                <?php } else { ?>
                                                    <i class="material-icons">clear</i>
                                                <?php } ?>
                                            </td>
                                            <td><?= $t->category_id; ?></td>
                                            <td><?= $t->category_name; ?></td>
                                            <td><?= $t->is_category_active; ?></td>
                                            <td><?= $t->product_data; ?></td>
                                        </tr>
                                    <?php } ?>


                                <?php } else { ?>
                                    <tr>
                                        <td colspan="7">No Data Available</td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>