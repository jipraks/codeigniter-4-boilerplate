<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Users</h4>
                <p class="card-category">Users Management</p>
            </div>
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12">
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#createUserModal"><i class="material-icons">add_circle_outline</i> Create User</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table id="usersTable" width="100%" class="display">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Full Name</th>
                                    <th>Role</th>
                                    <th>Last Login</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php if (count($usersData) > 0) { ?>

                                    <?php foreach ($usersData as $t) { ?>

                                        <tr>
                                            <td>
                                                <a href="#" onclick="getUserDetail('<?= $t->id; ?>')">
                                                    <?= $t->username; ?>
                                                </a>
                                            </td>
                                            <td><?= $t->fullname; ?></td>
                                            <td><?= $t->role; ?></td>
                                            <td><?= $t->last_login; ?></td>
                                            <td>
                                                <?php if ($t->is_active == 't') { ?>
                                                    <a href="#" onclick="deactivateUser('<?= $t->id; ?>', '<?= $t->username; ?>')">
                                                        <i class="material-icons">done</i>
                                                    </a>
                                                <?php } else { ?>
                                                    <a href="#" onclick="activateUser('<?= $t->id; ?>', '<?= $t->username; ?>')">
                                                        <i class="material-icons">clear</i>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>


                                <?php } else { ?>
                                    <tr>
                                        <td colspan="7">No Users Data Available</td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="createUserModal" tabindex="-1" role="dialog" aria-labelledby="createUserModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url('user/create'); ?>" method="POST">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createUserModal">Create New User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-5">
                        <label for="usernameInput" class="bmd-label-floating">Username</label>
                        <input type="text" class="form-control" id="usernameInput" name="username" required>
                        <span class="bmd-help">Username is Used for Login.</span>
                    </div>

                    <div class="form-group mb-5">
                        <label for="fullnameInput" class="bmd-label-floating">Fullname</label>
                        <input type="text" class="form-control" id="fullnameInput" name="fullname" required>
                        <span class="bmd-help">User's Full Name.</span>
                    </div>

                    <div class="form-group mb-5">
                        <label for="password1Input" class="bmd-label-floating">Password</label>
                        <input type="password" class="form-control" id="password1Input" name="password_1" required>
                        <span class="bmd-help">Strong Password That no One Knows.</span>
                    </div>

                    <div class="form-group mb-5">
                        <label for="password2Input" class="bmd-label-floating">Repeat Password</label>
                        <input type="password" class="form-control" id="password2Input" name="password_2" required>
                        <span class="bmd-help">Retype Your Strong Password. Just to Make Sure.</span>
                    </div>

                    <div class="form-group mb-5">
                        <label for="roleSelect">Select Roles</label>
                        <select class="form-control" id="roleSelect" name="role">
                            <option value="SYSADMIN">SYSADMIN</option>
                            <option value="HELPDESK">HELPDESK</option>
                            <option value="SPECTATOR">SPECTATOR</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create User</button>
                </div>
        </form>
    </div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="activationModal" tabindex="-1" role="dialog" aria-labelledby="activationModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="activationModal">Activation Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="activationMessage"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="#" id="activationButton" class="btn btn-primary"></a>
            </div>
        </div>
    </div>
</div>

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    function getUserDetail(user_id) {

        console.log(user_id);

        axios.get('<?= base_url(); ?>')
            .then((data) => {
                console.log(data);
            })

    }

    function deactivateUser(user_id, username) {

        document.getElementById('activationMessage').innerHTML = `You sure to <b>DEACTIVATE</b> ${username}?`;
        document.getElementById('activationButton').innerHTML = `DEACTIVATE ${username.toUpperCase()}`;
        document.getElementById('activationButton').href = `<?=base_url("user/deactivate");?>/${user_id}`;

        const options = {};
        $('#activationModal').modal(options)

    }

    function activateUser(user_id, username) {

        document.getElementById('activationMessage').innerHTML = `You sure to <b>ACTIVATE</a> ${username}?`;
        document.getElementById('activationButton').innerHTML = `ACTIVATE ${username.toUpperCase()}`;
        document.getElementById('activationButton').href = `<?=base_url("user/activate");?>/${user_id}`;

        const options = {};
        $('#activationModal').modal(options)

    }
</script>